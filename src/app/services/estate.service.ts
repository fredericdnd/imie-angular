import { Injectable } from '@angular/core';
import { Estate, EstateLocation, EstateInformations } from '../models/estate';

@Injectable({
	providedIn: 'root'
})
export class EstateService {

	estates: Estate[];
	
	constructor() {
		// Initialisation du tableau estates depuis le storage
		const datas = JSON.parse(localStorage.getItem("estates")) || [];
		this.estates = datas.map(data =>
			new Estate(data.id, data.location, data.informations)
		);
	}

	// Retourne le dernier id + 1
	incrementId(): number {
		return this.estates.length == 0 ?
			1 : this.estates[this.estates.length - 1].id + 1
			;
	}

	add(estate): void {
		let location = new EstateLocation(estate.address1, estate.address2, estate.postalCode, estate.city, estate.sector, estate.country, undefined, estate.latitude, estate.longitude);
		let informations = new EstateInformations(estate.photoUrl, estate.title, estate.description, estate.price, estate.surface, estate.owner, estate.reference);
		
		this.estates.push(
			new Estate(this.incrementId(), location, informations)
		);

		this.setToStorage();
	}

	remove(estate): void {
		this.estates.splice(this.estates.indexOf(estate), 1);
		this.setToStorage();
	}

	setToStorage() {
		localStorage.setItem(
			"estates",
			JSON.stringify(this.estates)
		);
	}

	getAll() {
		return this.estates;
	}

	findById(id): Estate {
		return this.estates.find(estate =>
			estate.id == id
		);
	}

}

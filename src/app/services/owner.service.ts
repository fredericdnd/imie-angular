import { Injectable } from '@angular/core';
import { Owner } from '../models/owner';

@Injectable({
	providedIn: 'root'
})
export class OwnerService {

	owners: Owner[];

	constructor() {
		// Initialisation du tableau owners depuis le storage
		const datas = JSON.parse(localStorage.getItem("owners")) || [];
		this.owners = datas.map(data =>
			new Owner(data.id, data.lastName, data.firstName, data.birthday, data.address1, data.address2, data.postalCode, data.city)
		);
	}

	// Retourne le dernier id + 1
	incrementId(): number {
		return this.owners.length == 0 ?
			1 : this.owners[this.owners.length - 1].id + 1
			;
	}

	add(owner): void {
		this.owners.push(
			new Owner(this.incrementId(), owner.lastName, owner.firstName, owner.birthday, owner.address1, owner.address2, owner.postalCode, owner.city)
		);

		this.setToStorage();
	}

	remove(owner): void {
		this.owners.splice(this.owners.indexOf(owner), 1);
		this.setToStorage();
	}

	update(owner): void {
		let ownerToRemove = this.findById(owner.id);
		this.remove(ownerToRemove);

		this.add(owner);
	}

	setToStorage() {
		localStorage.setItem(
			"owners",
			JSON.stringify(this.owners)
		);
	}

	getAll() {
		return this.owners;
	}

	findById(id): Owner {
		return this.owners.find(owner =>
			owner.id == id
		);
	}

}

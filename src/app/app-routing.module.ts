import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EstateCreateComponent } from './components/estate-create/estate-create.component';
import { EstateListComponent } from './components/estate-list/estate-list.component';
import { EstateDetailsComponent } from './components/estate-details/estate-details.component';
import { EstateMapComponent } from './components/estate-map/estate-map.component';
import { OwnerCreateComponent } from './components/owner-create/owner-create.component';
import { OwnerListComponent } from './components/owner-list/owner-list.component';

const routes: Routes = [
  { path: 'estate-create', component: EstateCreateComponent },
  { path: 'estate-create/:id', component: EstateCreateComponent },
  { path: 'estate-list', component: EstateListComponent },
  { path: 'estate-details/:id', component: EstateDetailsComponent },
  { path: 'estate-map', component: EstateMapComponent },
  { path: 'owner-create', component: OwnerCreateComponent },
  { path: 'owner-create/:id', component: OwnerCreateComponent },
  { path: 'owner-list', component: OwnerListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

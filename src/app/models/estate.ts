import { Owner } from './owner';

export class Estate {

        constructor(
                public id: number,
                public location: EstateLocation,
                public informations: EstateInformations
        ) { }

}

export class EstateLocation {

        constructor(
                public address1: string,
                public address2: string,
                public postalCode: string,
                public city: string,
                public sector: string,
                public country: string,
                public proximity: number,
                public latitude: number,
                public longitude: number
        ) { }

}

export class EstateInformations {

        constructor(
                public photoUrl: string,
                public title: string,
                public description: string,
                public price: number,
                public surface: number,
                public owner: Owner,
                public reference: string
        ) { }

}
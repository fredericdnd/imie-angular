export class Owner {

	constructor(	
		public id: number,
		public lastName: string,
		public firstName: string,
		public birthday: string,
		public address1: string,
		public address2: string,
		public postalCode: string,
		public city: string,
		public selected: boolean = false
	) {}

}
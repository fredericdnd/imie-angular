import { Component, OnInit, NgModule } from '@angular/core';
import { EstateService } from 'src/app/services/estate.service';
import { Estate } from 'src/app/models/estate';

@Component({
  selector: 'app-estate-list',
  templateUrl: './estate-list.component.html',
  styleUrls: ['./estate-list.component.scss']
})

export class EstateListComponent implements OnInit {

  estates: Estate[] = [];
  defaults: Estate[];

  constructor(
    private estateService: EstateService
  ) {
    this.defaults = this.estates;
    this.estates = this.estateService.getAll();
  }

  ngOnInit() {
  }

  remove(estate) {
    this.estateService.remove(estate);
  }

}

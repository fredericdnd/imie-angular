import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Estate } from 'src/app/models/estate';
import { EstateService } from 'src/app/services/estate.service';

@Component({
  selector: 'app-estate-details',
  templateUrl: './estate-details.component.html',
  styleUrls: ['./estate-details.component.scss']
})
export class EstateDetailsComponent implements OnInit {

  estate: Estate;

  constructor(
    private route: ActivatedRoute,
    private estateService: EstateService
  ) {
    let estateId: number

    this.route.params.subscribe( params => 
      this.estate = estateService.findById(params.id)
    );
  }

  ngOnInit() {
  }

}

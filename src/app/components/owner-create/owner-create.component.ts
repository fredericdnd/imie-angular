import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { OwnerService } from 'src/app/services/owner.service';
import { Owner } from 'src/app/models/owner';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-owner-create',
  templateUrl: './owner-create.component.html',
  styleUrls: ['./owner-create.component.scss'],
  providers: [ OwnerService ]
})
export class OwnerCreateComponent implements OnInit {

	owner: Owner;

  	constructor(
		private fb: FormBuilder,
		private ownerService: OwnerService,
		private route: ActivatedRoute
	) {
		let estateId: number

    	this.route.params.subscribe( params => 
      		estateId = params.id
		);

		this.owner = ownerService.findById(estateId)

		if (this.owner != undefined) {
			this.ownerForm.patchValue(this.owner);
		}
	}

	success : boolean = false;

	ownerForm = this.fb.group({
		lastName: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		firstName: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		birthday: ['', Validators.compose([
			Validators.required
		])],
		address1: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		address2: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		postalCode: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		city: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])]
	});

	submit() : void {
		if (this.owner == undefined) {
			this.ownerService.add(
				this.ownerForm.value
			);
		} else {
			this.ownerService.update(
				this.ownerForm.value
			);
		}

		this.ownerForm.reset();
		this.success = true;
	}

	ngOnInit() {
	}

}

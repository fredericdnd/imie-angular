import { Component, OnInit } from '@angular/core';
import { OwnerService } from 'src/app/services/owner.service';
import { Owner } from 'src/app/models/owner';

@Component({
  selector: 'app-owner-list',
  templateUrl: './owner-list.component.html',
  styleUrls: ['./owner-list.component.scss'],
  providers: [OwnerService]
})
export class OwnerListComponent implements OnInit {

  owners: Owner[] = [];
  defaults: Owner[];

  constructor(
    private ownerService: OwnerService
  ) {
    this.defaults = this.owners;
    this.owners = this.ownerService.getAll();
  }

  ngOnInit() {
  }

  remove(owner) {
    this.ownerService.remove(owner);
  }

}

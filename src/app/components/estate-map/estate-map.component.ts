import { Component, OnInit, NgModule } from '@angular/core';
import { EstateService } from 'src/app/services/estate.service';
import { Estate } from 'src/app/models/estate';

@Component({
  selector: 'app-estate-map',
  templateUrl: './estate-map.component.html',
  styleUrls: ['./estate-map.component.scss']
})

export class EstateMapComponent implements OnInit {

  estates: Estate[] = [];
  defaults: Estate[];

  constructor(
    private estateService: EstateService
  ) {
    this.defaults = this.estates;
    this.estates = this.estateService.getAll();

    console.log(this.estates);
  }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstateMapComponent } from './estate-map.component';

describe('EstateMapComponent', () => {
  let component: EstateMapComponent;
  let fixture: ComponentFixture<EstateMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstateMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstateMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

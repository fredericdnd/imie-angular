import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EstateService } from 'src/app/services/estate.service';
import { Estate } from 'src/app/models/estate';
import { Owner } from 'src/app/models/owner';
import { OwnerService } from 'src/app/services/owner.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-estate-create',
	templateUrl: './estate-create.component.html',
	styleUrls: ['./estate-create.component.scss']
})
export class EstateCreateComponent implements OnInit {

	owners: Owner[];
	estate: Estate;
	selectedOwnerId: number = 2;

	constructor(
		private fb: FormBuilder,
		private estateService: EstateService,
		private ownerService: OwnerService,
		private route: ActivatedRoute,
	) {
		this.owners = ownerService.getAll();

		let estateId: number

		this.route.params.subscribe(params =>
			estateId = params.id
		);

		this.estate = estateService.findById(estateId)

		if (this.estate != undefined) {
			// Patch location
			this.estateForm.patchValue({
				address1: this.estate.location.address1,
				address2: this.estate.location.address2,
				postalCode: this.estate.location.postalCode,
				city: this.estate.location.city,
				sector: this.estate.location.sector,
				country: this.estate.location.country,
				latitude: this.estate.location.latitude,
				longitude: this.estate.location.longitude
			});

			// Patch informations
			this.estateForm.patchValue({
				photoUrl: this.estate.informations.photoUrl,
				title: this.estate.informations.title,
				description: this.estate.informations.description,
				price: this.estate.informations.price,
				surface: this.estate.informations.surface,
				owner: this.estate.informations.owner,
				reference: this.estate.informations.reference,
			});
		}
	}

	success: boolean = false;

	estateForm = this.fb.group({
		address1: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		address2: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		postalCode: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		city: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		sector: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		country: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		latitude: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		longitude: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		photoUrl: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		title: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		description: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		price: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		surface: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])],
		owner: ['', Validators.required],
		reference: ['', Validators.compose([
			Validators.required,
			Validators.minLength(1)
		])]
	});

	submit(): void {
		const ownerId = this.estateForm.value.owner;
		this.estateForm.value.owner = this.ownerService.findById(ownerId);
		this.estateService.add(
			this.estateForm.value
		);

		this.estateForm.reset();
		this.success = true;
	}

	ngOnInit() {
	}

}

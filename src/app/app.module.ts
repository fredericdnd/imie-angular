import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgmCoreModule } from '@agm/core';

import { EstateListComponent } from './components/estate-list/estate-list.component';
import { EstateCreateComponent } from './components/estate-create/estate-create.component';
import { OwnerCreateComponent } from './components/owner-create/owner-create.component';
import { OwnerListComponent } from './components/owner-list/owner-list.component';
import { EstateDetailsComponent } from './components/estate-details/estate-details.component';
import { EstateMapComponent } from './components/estate-map/estate-map.component';

@NgModule({
  declarations: [
    AppComponent,
    EstateListComponent,
    EstateCreateComponent,
    OwnerCreateComponent,
    OwnerListComponent,
    EstateDetailsComponent,
    EstateMapComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC5NqcKKDNXy4sAsuKpH0NY3o22abmxa2s'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
